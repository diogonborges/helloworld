﻿using Nest;
using NuSearch.Domain.Data;
using System;
using System.Linq;
using NuSearch.Domain;

namespace NuSearch.Indexer
{
    class Program
	{
		private static ElasticClient Client { get; set; }
		private static NugetDumpReader DumpReader { get; set; }

		static void Main(string[] args)
		{
			Client = NuSearchConfiguration.GetClient();
			DumpReader = new NugetDumpReader(@"C:\nuget-data");

			IndexDumps();

			Console.Read();
		}

		static void IndexDumps()
		{
			var packages = DumpReader.Dumps.Take(1).First().NugetPackages;
			
			foreach (var package in packages)
			{
				var result = Client.Index(package);

				if (!result.IsValid)
				{
					Console.WriteLine(result.ConnectionStatus.OriginalException.Message);
					Console.Read();
					Environment.Exit(1);
				}
			}

			Console.WriteLine("Done.");
		}
	}
}
